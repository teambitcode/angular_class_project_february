import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-paragraph',
  templateUrl: './paragraph.component.html',
  styleUrls: ['./paragraph.component.scss']
})
export class ParagraphComponent implements OnInit {
  inputMyValue: string = '';
  constructor() {

  }

  ngOnInit(): void {
  }


  keypressEvent(event: any) {
    console.log(event.target.value);
  }

  clickMyButton() {
    console.log("Button clicked...!!!");
    console.log(this.inputMyValue);
  }


  selectDropdownEvent(event: any) {
    console.log(event.target.value);
  }
}
