import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TableComponent } from './table/table.component';
import { ListComponent } from './list/list.component';
import { ParagraphComponent } from './paragraph/paragraph.component';
import { FormsModule } from '@angular/forms';
import { LoginComponent } from './login/login.component';
import { LandingComponent } from './landing/landing.component';
import { ProjectTableComponent } from './project-table/project-table.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { ProjectPragraphComponent } from './project-pragraph/project-pragraph.component';
import { FilterDataPipe } from './filter-data.pipe';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    TableComponent,
    ListComponent,
    ParagraphComponent,
    LoginComponent,
    LandingComponent,
    ProjectTableComponent,
    NavBarComponent,
    ProjectPragraphComponent,
    FilterDataPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
