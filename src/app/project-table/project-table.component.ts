import { AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import { MainServiceService } from '../main-service.service';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-project-table',
  templateUrl: './project-table.component.html',
  styleUrls: ['./project-table.component.scss']
})
export class ProjectTableComponent implements OnInit, OnDestroy {

  searchText: string = '';

  studentArray: any[] = [];

  email: string = '';
  firstName: string = '';
  lastName: string = '';
  currentUserId: string = '';

  constructor(public mainServ: MainServiceService) {
  }
  ngOnDestroy(): void {
    console.log("ProjectPragraphComponent ngOnDestroy running");
  }

  ngOnInit(): void {
    console.log("started");

    this.getAllStudents();
    let result = this.mainServ.calculateData(12, 23);

  }

  getTableData(): any[] {
    console.log("***** getTableData");
    let tempTableData: any[] = [];
    // this.searchText

    for (let index = 0; index < this.studentArray.length; index++) {
      const element = this.studentArray[index];

      if (element.firstName.indexOf(this.searchText) != -1) {
        tempTableData.push(element);
      }
    }

    return tempTableData;
  }



  getAllStudents(): void {

    this.studentArray = [];

    this.mainServ.getApi("user/getAll").subscribe((responce: any) => {
      console.log(responce.data);
      this.studentArray = responce.data;
    });

    // this.http.get("http://64.227.28.237:5000/user/getAll").subscribe((responce:any)=>{
    //   console.log(responce.data);
    //   this.studentArray = responce.data;
    // });
    console.log('next line 1');
    console.log('next line 2');
  }

  setUserDataToForm(userData: any) {
    console.log(userData);
    this.email = userData.email;
    this.firstName = userData.first_name;
    this.lastName = userData.last_name;

    this.currentUserId = userData._id;
  }

  saveChanges() {

    let userUpdateBody = {
      'email': this.email,
      'first_name': this.firstName,
      'last_name': this.lastName
    };

    this.mainServ.patchApi("user/update/" + this.currentUserId, userUpdateBody).subscribe((responce: any) => {
      console.log(responce);
      this.getAllStudents();
      Swal.fire({
        position: 'top-end',
        icon: 'success',
        title: 'User updated successfully',
        showConfirmButton: false,
        timer: 1500
      });

    }, (errorResponce: any) => {
      console.log(errorResponce);

      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Something went wrong!'
      });

    });

  }


  clickOnDelete(userData: any){

    this.currentUserId = userData._id;
    Swal.fire({
      title: 'Are you sure?',
      text: "Do you want to delete this user?",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {

        console.log("delete confirmed");
        this.deleteUser();
      }
    })
  }


  deleteUser(){
    this.mainServ.deleteApi("user/remove/" + this.currentUserId).subscribe((responce: any) => {
      console.log(responce);
      this.getAllStudents();
      Swal.fire({
        position: 'top-end',
        icon: 'success',
        title: 'User deleted successfully',
        showConfirmButton: false,
        timer: 1500
      });

    }, (errorResponce: any) => {
      console.log(errorResponce);

      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Something went wrong!'
      });

    });
  }

}
