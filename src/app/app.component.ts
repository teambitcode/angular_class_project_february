import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'my-dream-app';

  studentArray: any[] = [
    {
      firstName: "Amal",
      lastName: "Ddaf sdaf",
      age: 76,
    },
    {
      firstName: "Gayan",
      lastName: "asdsad fddsflsdf",
      age: 45,
    },
    {
      firstName: "Huyads",
      lastName: "ds ghfjfd",
      age: 65,
    },
    {
      firstName: "jkhfjkf",
      lastName: "bndn sd",
      age: 15,
    }
  ];

  getData(event: any) {
    console.log(event);
  }

}
