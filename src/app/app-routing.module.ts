import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LandingComponent } from './landing/landing.component';
import { ListComponent } from './list/list.component';
import { LoginComponent } from './login/login.component';
import { ParagraphComponent } from './paragraph/paragraph.component';
import { ProjectTableComponent } from './project-table/project-table.component';

import { TableComponent } from './table/table.component';

const landingChildRoutes : Routes = [
  {
    path: 'table',
    component: ProjectTableComponent
  },
  {
    path: 'list',
    component: ListComponent
  }
];

const routes: Routes = [
  {
    path:'',
    component: LoginComponent
  },
  {
    path:'login',
    component: LoginComponent
  },
  {
    path:'landing',
    component: LandingComponent,
    children: landingChildRoutes
  }
];






@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {


 }
