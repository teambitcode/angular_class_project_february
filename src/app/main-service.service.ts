import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MainServiceService {

  userName: string = "";


  constructor(private http: HttpClient) { }


  calculateData(number1: number, number2: number) {
    return number1 + number2;
  }


  getApi(endpoint: string) {
    // let url = "http://64.227.28.237:5000/" + endpoint;
    let url = environment.requestPrefix + endpoint;
    return this.http.get(url);
  }

  postApi(endpoint: string, requestBody: any) {
    let url = environment.requestPrefix + endpoint;
    return this.http.post(url, requestBody);
  }

  patchApi(endpoint: string, requestBody: any) {
    let url = environment.requestPrefix + endpoint;
    return this.http.patch(url, requestBody);
  }

  deleteApi(endpoint: string) {
    let url = environment.requestPrefix + endpoint;
    return this.http.delete(url);
  }


}
