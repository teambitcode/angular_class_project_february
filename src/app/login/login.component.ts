import { Component, OnInit } from '@angular/core';
import { MainServiceService } from '../main-service.service';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  isLoginForm: boolean = true;

  email: string = '';
  password: string = '';
  firstName: string = '';
  lastName: string = '';
  role: string = 'user';

  constructor(private mainServ: MainServiceService, private router: Router) { }




  ngOnInit(): void {
  }

  setUserData(): void {
    this.mainServ.userName = "sdfsdf(******";
  }

  gotoRegister() {
    this.isLoginForm = false;
  }

  gotoLogin() {
    this.isLoginForm = true;
  }

  registerUser() {
    console.log(this.email);
    console.log(this.password);
    console.log(this.firstName);
    console.log(this.lastName);
    console.log(this.role);

    let userCreateBody = {
      'email': this.email,
      'password': this.password,
      'first_name': this.firstName,
      'last_name': this.lastName,
      'role': this.role
    };

   

    this.mainServ.postApi("user/new",userCreateBody).subscribe((responce: any) => {
      console.log(responce);
      Swal.fire( {
        position: 'top-end',
        icon: 'success',
        title: 'User created successfully',
        showConfirmButton: false,
        timer: 1500
      });

    }, (errorResponce: any) => {
      console.log(errorResponce);

      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Something went wrong!'
      });

    });

    // this.http.post("http://64.227.28.237:5000/user/new", userCreateBody)


  }

  login(){

    console.log(this.email);
    console.log(this.password);

    let userLoginBody = {
      'email': this.email,
      'password': this.password
    };


    this.mainServ.postApi("user/login",userLoginBody).subscribe((responce: any) => {
      console.log(responce);
      Swal.fire( {
        position: 'top-end',
        icon: 'success',
        title: 'User logged successfully',
        showConfirmButton: false,
        timer: 1500
      });

      this.router.navigateByUrl("/landing/table");

      // navigate /landing/table


    }, (errorResponce: any) => {
      console.log(errorResponce.error.msg);

      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: errorResponce.error.msg
      });

    });

    // this.http.post("http://64.227.28.237:5000/user/login", userLoginBody)
  }

  setUserRole(event: any) {
    console.log(event.target.value);
    this.role = event.target.value;
  }

}
