import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterData'
})
export class FilterDataPipe implements PipeTransform {

  transform(valueArray: any[], searchValue:string ) {
    
    console.log("***** FilterDataPipe");
    let tempTableData: any[] = [];

    for (let index = 0; index < valueArray.length; index++) {
      const element = valueArray[index];

      if(element.first_name.indexOf(searchValue) != -1 ){
        tempTableData.push(element);
      }
    }

    return tempTableData;
    
  }

}
