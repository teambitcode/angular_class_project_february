import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss']
})
export class LandingComponent implements OnInit {
  studentArray: any[] = [
    {
      firstName: "Amal",
      lastName: "Ddaf sdaf",
      age: 76,
    },
    {
      firstName: "Kamal",
      lastName: "asdsad fddsflsdf",
      age: 45,
    },
    {
      firstName: "Saman",
      lastName: "ds ghfjfd",
      age: 65,
    },
    {
      firstName: "Namal",
      lastName: "bndn sd",
      age: 15,
    }
  ];

  textInput: string = "****";
  constructor() { }

  ngOnInit(): void {
  }

  setTextInput(event: any) {
    console.log(event.target.value);

    this.textInput = event.target.value;
  }
}
