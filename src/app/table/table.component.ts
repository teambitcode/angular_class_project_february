import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {
  @Input() inputStudentArray: any[] = [];
  @Input() inputName: any = '';

  @Output() outStudentArray = new EventEmitter();

  myValue = 29;
  selectedValue1: string = 'Onedddddd';

  constructor() { }

  ngOnInit(): void {
  }

  clickTabaleData(data: any) {
    this.outStudentArray.emit(data);
  }
}
