import { AfterViewInit, Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-project-pragraph',
  templateUrl: './project-pragraph.component.html',
  styleUrls: ['./project-pragraph.component.scss']
})
export class ProjectPragraphComponent implements OnInit, AfterViewInit, OnChanges {

  @Input() paragraphData : string = "";

  @Input() title : string = "";

  constructor() {
    // console.log("ProjectPragraphComponent constructor running");
   }
    
  ngOnInit(): void {
    // console.log("ProjectPragraphComponent ngOnInit running");
  }
  
  ngAfterViewInit(): void {
    // console.log("ProjectPragraphComponent ngAfterViewInit running");
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log(changes);
  }


}
