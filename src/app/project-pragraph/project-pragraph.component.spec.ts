import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectPragraphComponent } from './project-pragraph.component';

describe('ProjectPragraphComponent', () => {
  let component: ProjectPragraphComponent;
  let fixture: ComponentFixture<ProjectPragraphComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProjectPragraphComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectPragraphComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
